﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    float throwForce = 600;
    Vector3 objectPos;
    float distance;

    public bool canHold = true;
    public GameObject item;
    public GameObject tempParent;
    public GameObject handParent;
    public bool isHolding = false;
    public bool HandHolding;

    //public Transform theDest;
    void Update()
    {
        distance = Vector3.Distance(item.transform.position, tempParent.transform.position);
        if (distance >= 1f && !HandHolding)
        {
            isHolding = false;
        }
        if (isHolding == true)
        {
            item.GetComponent<Rigidbody>().velocity = Vector3.zero;
            item.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            if (HandHolding)
            {
                item.transform.SetParent(handParent.transform);
                item.transform.localPosition = Vector3.zero;
            }
            else
                item.transform.SetParent(tempParent.transform);

            if (Input.GetKeyDown(KeyCode.A))
            {
                item.GetComponent<Rigidbody>().AddForce(tempParent.transform.forward * throwForce);
                isHolding = false;
            }
        }
        else
        {
            objectPos = item.transform.position;
            item.transform.SetParent(null);
            item.GetComponent<Rigidbody>().useGravity = true;
            item.transform.position = objectPos;
        }
    }
    void OnMouseDown()
    {
        if (distance <= 1f)
        {
            Pickup();
        }
        //GetComponent<BoxCollider>().enabled = false;
        //GetComponent<Rigidbody>().useGravity = false;
        //this.transform.position = theDest.position;
        //this.transform.parent = GameObject.Find("Destination").transform;
    }

    void OnMouseUp(){

        isHolding = false;
        //this.transform.parent = null;
        //GetComponent<Rigidbody>().useGravity = true;
        //GetComponent<BoxCollider>().enabled = true;
    }


    public void Pickup(){

        isHolding = true;
        item.GetComponent<Rigidbody>().useGravity = false;
        item.GetComponent<Rigidbody>().detectCollisions = true;
    }
}
