﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Climbing : MonoBehaviour
{
    public float Walkspeed = 5f;
    public float Climbspeed = 3f;
    public LayerMask wallMask;

    bool climbing;

    Vector3 wallPoint;
    Vector3 wallNormal;

    Rigidbody body;
    CapsuleCollider coll;


    // Start is called before the first frame update
    void Start()
    {
        body = GetComponent<Rigidbody>();
        coll = GetComponent<CapsuleCollider>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (NearWall())
        {
            if (FacingWall())
            {
                if (Input.GetKeyUp(KeyCode.A))
                {
                    climbing = !climbing;
                }
            }
            else
            {
                climbing = false;
            }
        }
        else
        {
            climbing = false;
        }

        if (climbing)
        {
            ClimbWall();
        }
        else
        {
            Walk();
        }
    }

    void Walk()
    {
        body.useGravity = true;

        var v = Input.GetAxis("Vertical");
        var h = Input.GetAxis("Horizontal");
        var move = transform.forward * v + transform.right * h;

        ApplyMove(move, Walkspeed);
    }

    bool NearWall()
    {
        return Physics.CheckSphere(transform.position, 3f, wallMask);
    }

    bool FacingWall()
    {
        RaycastHit hit;
        var facingWall = Physics.Raycast(transform.position, transform.forward, out hit, coll.radius + 1f, wallMask);
        wallPoint = hit.point;
        wallNormal = hit.normal;
        return facingWall;
    }

    void ClimbWall()
    {
        body.useGravity = false;

        GrabWall();

        var v = Input.GetAxis("Vertical");
        var h = Input.GetAxis("Horizontal");
        var move = transform.up * v + transform.right * h;

        ApplyMove(move, Climbspeed);
    }

    void GrabWall()
    {
        var newPosition = wallPoint + wallNormal * (coll.radius - 0.1f);
        transform.position = Vector3.Lerp(transform.position, newPosition, 10 * Time.deltaTime);

        if (wallNormal == Vector3.zero)
            return;

        transform.rotation = Quaternion.LookRotation(-wallNormal);
    }

    void ApplyMove(Vector3 move, float speed)
    {
        body.MovePosition(transform.position + move * speed * Time.deltaTime);
    }
}
