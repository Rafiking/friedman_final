﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hands : MonoBehaviour
{
    Rigidbody rb;
    public Transform[] pieces;
    public Transform dropoff;
    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        StartCoroutine(HandAI());
    }

    // Update is called once per frame
    void Update()
    {

    }
    IEnumerator HandAI()
    {
        while (true)
        {
            Transform target = null;
            bool found = false;
            while (!found)
            {
                print("CHECK FOUND!");
                int index = (int)(pieces.Length * Random.value);
                if (Vector3.Distance(pieces[index].position, dropoff.position) > 25f)
                {
                    print("FOUND!");
                    target = pieces[index];
                    found = true;
                }

                yield return new WaitForSeconds(1f);
            }




            bool grabbed = false;
            while (!grabbed && target != null)
            {
                print("MOVE!" + Vector3.Distance(transform.position, target.position));
                Vector3 dir = target.position - transform.position;
                rb.velocity += dir * speed * Time.deltaTime;


                if (Vector3.Distance(transform.position, target.position) < 7f)
                {
                    print("GRAB|!");
                    grabbed = true;
                    //target.SetParent(transform);
                    target.GetComponent<PickUp>().HandHolding = true;
                    //print("Post parent:? " + target.parent.name);
                    target.GetComponent<PickUp>().Pickup();

                }
                yield return null;
            }


            while (grabbed == true)
            {
                Vector3 overDrop = dropoff.position + Vector3.up * 20;
                Vector3 dir = overDrop - transform.position;
                rb.velocity += dir * speed * Time.deltaTime;

                if (Vector3.Distance(transform.position, overDrop) < 5f)
                {
                    print("I'm Here");
                    grabbed = false;
                    target.GetComponent<PickUp>().HandHolding = false;
                }
                yield return null;
            }

            yield return new WaitForSeconds(3f);
        }
    }
}
