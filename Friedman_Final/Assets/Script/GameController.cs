﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour

{
    public static GameController gc;

    public bool IsPlaced;

    public int Score;

    void Awake()
    {
        if (gc == null)
        {
            gc = this;

            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
    }
    public void AddScore(int toAdd)
    {
        Score += toAdd;
        print(Score);
    }
}
