﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Score : MonoBehaviour
{
    public Text scoreboard;
    public float TimeLeft = 120f;
    public Image GameOver;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        TimeLeft -= Time.deltaTime;
        if (TimeLeft > 110)
        {
            scoreboard.text = "Click to pick up, press A to throw!";
        }
        if (TimeLeft < 110)
        {
            scoreboard.text = "Score:" + GameController.gc.Score.ToString() + " Time:" + (TimeLeft).ToString("0");
        }
        if (TimeLeft < 0)
        {
            print("Game Over");
            GameOver.gameObject.SetActive(true);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.relativeVelocity.magnitude > 3f)
        {
            int score = (int)(collision.relativeVelocity.magnitude * 1000f);
            GameController.gc.AddScore(score);
        }
    }
}
